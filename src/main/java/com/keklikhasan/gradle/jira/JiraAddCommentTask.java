package com.keklikhasan.gradle.jira;

import net.rcarz.jiraclient.*;
import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;

import org.gradle.api.logging.Logger;

import java.util.List;

public class JiraAddCommentTask extends DefaultTask {

    private JiraPluginExtension jiraPluginExtension;
    private List<JiraAddCommentExtension> jiraAddCommentExtensions;
    private Logger logger;

    public JiraAddCommentTask() {
        super();
        this.setDescription("Add comment to a jira issue");
        this.setGroup(JiraPlugin.GROUP_NAME);
        logger = getLogger();
    }

    @TaskAction
    public void comment() {
        jiraPluginExtension = (JiraPluginExtension) getProject().getExtensions().findByName("jira");
        jiraAddCommentExtensions = jiraPluginExtension.addCommentExtensions;
        if (!validateJiraExtension()) {
            return;
        }

        if (jiraAddCommentExtensions == null || jiraAddCommentExtensions.isEmpty()) {
            logger.error("AddCommentExtensions is empty");
            return;
        }

        BasicCredentials credentials = new BasicCredentials(jiraPluginExtension.userName, jiraPluginExtension.password);
        JiraClient jira = new JiraClient(jiraPluginExtension.url, credentials);

        for (JiraAddCommentExtension jiraAddCommentExtension : jiraAddCommentExtensions) {
            if (validateJiraAddCommentExtension(jiraAddCommentExtension)) {
                try {
                    Issue issue = jira.getIssue(jiraAddCommentExtension.taskKey);
                    issue.addComment(jiraAddCommentExtension.comment);
                    logger.info(String.format("Comment %s added to %s task",
                            jiraAddCommentExtension.comment, jiraAddCommentExtension.taskKey));
                } catch (JiraException ex) {
                    logger.error(String.format("Error comment %s added to %s task",
                            jiraAddCommentExtension.comment, jiraAddCommentExtension.taskKey), ex);
                }
            }
        }
    }

    private boolean validateJiraExtension() {
        String url = jiraPluginExtension.url;
        if (TextUtils.isEmpty(url)) {
            logger.error("url is empty");
            return false;
        }
        if (!url.startsWith("http")) {
            logger.error("url must be url and start with http or https");
            return false;
        }
        if (url.endsWith("/")) {
            jiraPluginExtension.url = url.substring(0, url.length() - 1);
        }
        if (TextUtils.isEmpty(jiraPluginExtension.userName)) {
            logger.error("userName is empty");
            return false;
        }
        if (TextUtils.isEmpty(jiraPluginExtension.password)) {
            logger.error("password is empty");
            return false;
        }
        return true;
    }

    private boolean validateJiraAddCommentExtension(JiraAddCommentExtension jiraAddCommentExtension) {
        if (jiraAddCommentExtension == null) {
            logger.error("addCommentExtension properties empty");
            return false;
        }
        if (TextUtils.isEmpty(jiraAddCommentExtension.taskKey)) {
            logger.error("taskKey is empty");
            return false;
        }
        if (TextUtils.isEmpty(jiraAddCommentExtension.comment)) {
            logger.error("comment is empty");
            return false;
        }
        return true;
    }

}

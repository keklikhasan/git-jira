package com.keklikhasan.gradle.jira;


public final class TextUtils {

    private TextUtils() {
    }

    public static boolean isEmpty(CharSequence s) {
        return s == null || s.length() == 0;
    }
}

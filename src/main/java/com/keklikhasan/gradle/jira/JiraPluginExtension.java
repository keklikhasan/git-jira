package com.keklikhasan.gradle.jira;

import groovy.lang.Closure;
import org.gradle.api.Project;

import java.util.ArrayList;
import java.util.List;

public class JiraPluginExtension {
    public String url;
    public String userName;
    public String password;
    public List<JiraAddCommentExtension> addCommentExtensions = new ArrayList<>();

    private final Project project;

    public JiraPluginExtension(Project project) {
        this.project = project;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<JiraAddCommentExtension> getAddCommentExtensions() {
        return addCommentExtensions;
    }

    public void setAddCommentExtensions(List<JiraAddCommentExtension> addCommentExtensions) {
        this.addCommentExtensions = addCommentExtensions;
    }

    public void addCommentExtension(Closure<JiraAddCommentExtension> addCommentExtension) throws Exception {
        JiraAddCommentExtension commentExtension = new JiraAddCommentExtension();
        addCommentExtension.setDelegate(commentExtension);
        addCommentExtension.call();
        addCommentExtensions.add(commentExtension);
    }
}

package com.keklikhasan.gradle.jira;

public class JiraAddCommentExtension {
    public String taskKey;
    public String comment;

    public JiraAddCommentExtension() {
    }

    public void taskKey(String taskKey) {
        this.taskKey = taskKey;
    }

    public void comment(String comment) {
        this.comment = comment;
    }
}

package com.keklikhasan.gradle.jira;

import org.gradle.api.Plugin;
import org.gradle.api.Project;

public class JiraPlugin implements Plugin<Project> {

    final static String GROUP_NAME = "jira";

    @Override
    public void apply(Project project) {
        applyExtensions(project);
        applyTasks(project);
    }

    private void applyExtensions(final Project project) {
        project.getExtensions().create("jira", JiraPluginExtension.class, project);
    }

    private void applyTasks(final Project project) {
        project.getTasks().create("addJiraComment", JiraAddCommentTask.class);
    }
}
